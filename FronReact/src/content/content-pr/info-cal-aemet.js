import React from "react";
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Image from 'react-bootstrap/Image';

const  temperatura_max  = process.env.PUBLIC_URL + "/assets/svg/043-warm.svg";
const  temperatura_min  =  process.env.PUBLIC_URL + "/assets/svg/042-cold.svg";
const  lluvia_hoy =  process.env.PUBLIC_URL + "/assets/svg/025-humidity.svg";

const  svg = { "11":{"v":"039-sun"}, "11n": { "v":"022-night-3"}, "12":{ "v": "038-cloudy-3"}, "12n":{ "v":"007-night"},
"13":{ "v": "038-cloudy-3"}, "13n":{ "v":"007-night"}, "14":{ "v": "038-cloudy-3"}, "14n":{ "v":"007-night"},
"15":{ "v":"011-cloudy"}, "16":{ "v":"011-cloudy"},"17":{ "v":"039-sun"}, "17n":{ "v":"022-night-3"},
"43":{ "v":"034-cloudy-1"}, "43n":{ "v":"021-night-2"},"44":{ "v":"034-cloudy-1"}, "44n":{ "v":"021-night-2"},
"46":{ "v":"003-rainy"}, "45":{ "v":"003-rainy"},"23":{ "v":"034-cloudy-1"}, "23n":{ "v":"021-night-2"},
"24":{ "v":"034-cloudy-1"}, "24n":{ "v":"021-night-2"},"25":{ "v":"004-rainy-1"}, "26":{ "v":"004-rainy-1"},"0":{ "v":"008-storm"}};


export default class InfoAemet extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {current: null, rainProb: null, forecast: [], clima: null, clima_1: null, clima_2: null}
    }
    
    componentDidMount() {
        fetch("http://192.168.18.16:3000/AEMET")
        .then(res => res.json())
        .then(json => {
            var hoy = undefined ? "0" : json['today']['value'];
            var mañana =  undefined ? "0" : json['tomorrow']['value'];
            var pasado =  undefined ? "0" : json['next2']['value'];
            
            this.setState(
                {
                current: json['today']['tmp']['current'],
                rainProb: json['today']['rainProb'],
                forecast: json['forecast'], 
                clima   : process.env.PUBLIC_URL + "/assets/svg/" + svg[hoy]["v"] + ".svg",
                clima_1  : process.env.PUBLIC_URL + "/assets/svg/" + svg[mañana]["v"] + ".svg",
                clima_2 : process.env.PUBLIC_URL + "/assets/svg/" + svg[pasado]["v"] + ".svg"
            });

        })
    }

    render() {
        if(this.state.clima !== null){
        return(
            <Container>
                <Row className=" bd-highlight justify-content-md-center ">
                    <Col align="center" className="mt-1 mb-0" >
                        <Image className="img_1" src={this.state.clima}></Image>
                    </Col>
                    <Col className="col align-self-center  mt-0">
                        <Row  className = "mt-3 mb-0  p-0 d-flex flex-nowrap " > 
                            <h4 id="Color_Text" align="center" class = "m-0 p-0" > {this.state.current} Cº <Image className="img_2"  src={temperatura_max}></Image> </h4>
                        </Row>
                        <Row  className = "mt-0 mb-3 p-0  d-flex flex-nowrap "> 
                            <h4 id="Color_Text" align="center" class = "m-0 p-0" > {this.state.rainProb} % <Image className="img_2" src={lluvia_hoy}></Image></h4>
                        </Row>
                    </Col>
                </Row>

                <Row>
                    {
                        
                        this.state.forecast.slice(1,4).map(ele => (
                            <Col>
                                <Row >
                                    <h5 id="Color_Text" class=" align-self-center  m-0" align="center"> {ele['$']['fecha'].split('-')[2]}
                                        <Image className="img_2 p-0 " src={this.state.clima}></Image>
                                    </h5>
                                </Row>
                                <Row >
                                    <p  id="Color_Text" class=" align-self-center  m-0" align="center"> {ele['temperatura']['maxima']} ºC
                                        <Image className="img_3 p-0 " src={temperatura_max}></Image>
                                    </p>
                                </Row>
                                <Row >
                                    <p id="Color_Text" class=" align-self-center  m-0" align="center"> {ele['temperatura']['minima']} ºC
                                        <Image className="img_3 p-0 " src={temperatura_min}></Image>
                                    </p>
                                </Row>
                                <Row >
                                    <p id="Color_Text" class=" align-self-center  m-0" align="center"> {ele['prob_precipitacion'][0]["_"]} %
                                        <Image className="img_3 p-0 " src={lluvia_hoy}></Image>
                                    </p>
                                </Row>
                            </Col>
                        ))
                    }
                </Row>
                
            </Container>
        );} else {
            return(<Container></Container>);
        }
    }
}
