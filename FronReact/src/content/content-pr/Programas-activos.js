const pogramas_activos = ({ l_programas_activos }) => {

    return(
        <div className="card">
            <div className="Heart-card"> 
                <h2> Pogramados Hoy </h2>
            </div>
            <div className="Content-card">  
                <table className=" table ">
                    <thead>
                        <th>ID</th>
                        <th>Inicio</th>
                        <th>Final</th>
                        <th>Estao</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Prog1</td>
                            <td>00:00</td>
                            <td>10:00</td>
                            <td>ON</td>
                        </tr>
                        <tr>
                            <td>Prog2</td>
                            <td>02:10</td>
                            <td>03:10</td>
                            <td>OFF</td>
                        </tr>
                        <tr>
                            <td>Prog2</td>
                            <td>02:10</td>
                            <td>03:10</td>
                            <td>OFF</td>
                        </tr>
                        <tr>
                            <td>Prog2</td>
                            <td>02:10</td>
                            <td>03:10</td>
                            <td>OFF</td>
                        </tr>
                        <tr>
                            <td>Prog2</td>
                            <td>02:10</td>
                            <td>03:10</td>
                            <td>OFF</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    );
}

export default pogramas_activos;