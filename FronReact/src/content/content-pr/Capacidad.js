const Capacidad = ({ l_programas_activos }) => {

    return(
        <div className="card">
            <div className="Heart-card"> 
                <h2> Capacidad  </h2>
            </div>
            <div className="Content-card tanques">
                <div className="t-individual">
                    <p>T.1</p>
                    <div class="wrapper">
                        <div class="wave"></div>
                        <p>20%</p>
                    </div>
                </div>
                <div className="divisor"></div>
                <div className="t-individual">
                    <p>T.2</p>
                    <div class="wrapper">
                        <div class="wave"></div>
                        <p>20%</p>
                    </div>
                </div>
                

            </div>
        </div>
    );
}

export default Capacidad;