import React from "react";
const  temperatura_max  = process.env.PUBLIC_URL + "/assets/svg/043-warm.svg";
const  temperatura_min  =  process.env.PUBLIC_URL + "/assets/svg/042-cold.svg";
const  lluvia_hoy =  process.env.PUBLIC_URL + "/assets/svg/025-humidity.svg";

const  svg = { "11":{"v":"039-sun"}, "11n": { "v":"022-night-3"}, "12":{ "v": "038-cloudy-3"}, "12n":{ "v":"007-night"},
"13":{ "v": "038-cloudy-3"}, "13n":{ "v":"007-night"}, "14":{ "v": "038-cloudy-3"}, "14n":{ "v":"007-night"},
"15":{ "v":"011-cloudy"}, "16":{ "v":"011-cloudy"},"17":{ "v":"039-sun"}, "17n":{ "v":"022-night-3"},
"43":{ "v":"034-cloudy-1"}, "43n":{ "v":"021-night-2"},"44":{ "v":"034-cloudy-1"}, "44n":{ "v":"021-night-2"},
"46":{ "v":"003-rainy"}, "45":{ "v":"003-rainy"},"23":{ "v":"034-cloudy-1"}, "23n":{ "v":"021-night-2"},
"24":{ "v":"034-cloudy-1"}, "24n":{ "v":"021-night-2"},"25":{ "v":"004-rainy-1"}, "26":{ "v":"004-rainy-1"},"0":{ "v":"008-storm"}};


export default class Aemet extends React.Component {
    
    
    constructor(props) {
        super(props);
        this.state = {current: null, rainProb: null, forecast: [], clima: null, clima_1: null, clima_2: null}
    }
    
    componentDidMount() {
        fetch("http://192.168.18.16:3000/AEMET")
        .then(res => res.json())
        .then(json => {
            var hoy = undefined ? "0" : json['today']['value'];
            var mañana =  undefined ? "0" : json['tomorrow']['value'];
            var pasado =  undefined ? "0" : json['next2']['value'];
            
            this.setState(
                {
                current: json['today']['tmp']['current'],
                rainProb: json['today']['rainProb'],
                forecast: json['forecast'], 
                clima   : [process.env.PUBLIC_URL + "/assets/svg/" + svg[hoy]["v"] + ".svg",
                process.env.PUBLIC_URL + "/assets/svg/" + svg[mañana]["v"] + ".svg",
                process.env.PUBLIC_URL + "/assets/svg/" + svg[pasado]["v"] + ".svg"]
            });

        })
    }

    render() {
        if(this.state.clima !== null){
        return(
            <div className="card">
                <div className="Heart-card"> 
                    <h2> El Tiempo  </h2>
                </div>
                <div className="Content-card">  
                    <div className = "container">
                        <div className="container-tab">
                            <div className = "icon"> 
                                <img className="img_1" src={this.state.clima[0]}></img>
                                <div>
                                    <p> { this.state.current } ºC <img className="img_2" src={temperatura_max}></img> </p> 
                                    <p> { this.state.rainProb } % <img className="img_2" src={lluvia_hoy}></img> </p> 
                                </div>
                            </div>
                            <div className = " other-time ">
                                    {
                                    
                                    this.state.forecast.slice(1,4).map((ele, index) => (
                                        <div>
                                            <h2 align="center" > {ele['$']['fecha'].split('-')[2]} 
                                                <img className="img_2 " src={this.state.clima[index]}></img>
                                            </h2>
                                            <p align="center"> {ele['temperatura']['maxima']} ºC
                                                <img className="img_3" src={temperatura_max}></img>
                                            </p>
                                            <p align="center"> {ele['temperatura']['minima']} ºC
                                                <img className="img_3" src={temperatura_min}></img>
                                            </p>
                                            <p align="center"> {ele['prob_precipitacion'][0]["_"]} %
                                                <img className="img_3" src={lluvia_hoy}></img>
                                            </p>
                                        </div>
                                        
                                    ))
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        );} else {
            return(<div></div>);
        }
    }
}
