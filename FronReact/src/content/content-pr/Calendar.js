import Calendar from 'react-calendar';

const Calendario = ({ l_programas_activos }) => {

    return(
        <div className="card">
            <div className="Heart-card"> 
                <h2> Calendario  </h2>
            </div>
            <div className="Content-card">  
               <Calendar></Calendar>
            </div>
        </div>
    );
}

export default Calendario;