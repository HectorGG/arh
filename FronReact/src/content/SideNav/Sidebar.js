import React,  { useState, useEffect } from 'react';
import { ProSidebar, SidebarHeader, SidebarFooter, SidebarContent } from 'react-pro-sidebar';
import { Menu, MenuItem, SubMenu } from 'react-pro-sidebar';
import { FaDashcube, FaProductHunt } from 'react-icons/fa';

import {
    Link
  } from "react-router-dom";

import Moment from 'moment';

const Sidebar  = ({programas, sectores, routes}) =>  {
    const [date, setDate] = useState(1);

    useEffect(() => {
        setInterval(() => {
        setDate(Moment( Date.now()).format('HH:mm:ss  DD MMM'));
        })
    })


    var render_programas  = programas.map(ele =>  <MenuItem> <Link to={ele.name}> {ele.name}</Link> </MenuItem>);
    var render_sectores =  sectores.map(ele =>   <MenuItem> {ele.name} </MenuItem>);

    return(
        <ProSidebar>
        <SidebarHeader>
            <h1>Riego Arduino</h1>
        </SidebarHeader>
        <SidebarContent>
        <Menu iconShape="circle">
            <MenuItem icon={<FaDashcube />}><Link to="/Estados">Estados</Link></MenuItem>
            <SubMenu title="Programas" icon={<FaProductHunt />} suffix={<span className="badge yellow">{programas.length}</span>} >
                { render_programas }
            </SubMenu>
            <SubMenu title="Sectores" icon={<FaDashcube />} suffix={<span className="badge yellow">{sectores.length}</span>} >
                { render_sectores }
            </SubMenu>
            <MenuItem icon={<FaDashcube />}><Link to="/Caudal">Caudal</Link></MenuItem>
            <MenuItem icon={<FaDashcube />}><Link to="/Capacidad">Capacidad</Link></MenuItem>
            <MenuItem icon={<FaDashcube />}><Link to="/PH_Conductimetro">PH Conductimetro</Link></MenuItem>
            <MenuItem icon={<FaDashcube />}><Link to="/Sensores">Sensores</Link></MenuItem>
        </Menu>
        </SidebarContent>
        <SidebarFooter>
           
        </SidebarFooter>
      </ProSidebar>
    );

    
}

export default Sidebar;