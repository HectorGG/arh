import ProgramasAC from './content-pr/Programas-activos';
import Aemet from './content-pr/Aemet';
import Calendar from './content-pr/Calendar';
import Capacidad from './content-pr/Capacidad';
import Caudal from './content-pr/Caudal';
import Contador from './content-pr/Contador';
import PH_Conductimetro from './content-pr/Ph_Condu';
import Sectors from './content-pr/Sectors';
import Telemetria from './content-pr/Telemetria';

const pogramas_activos = ({ l_programas_activos }) => {

    return(
        <div>
       <div className="container">
            <ProgramasAC l_programas_activos = {"Hola"}></ProgramasAC> 
            <Aemet></Aemet>
            <Calendar></Calendar>
        </div>
         <div className="container">
            <Capacidad></Capacidad>
            <Caudal></Caudal>
            <Contador></Contador>
            <PH_Conductimetro></PH_Conductimetro>
            <Sectors></Sectors>
        </div>
        </div>
        
    );
}

export default pogramas_activos;