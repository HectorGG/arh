import React, { useState, useEffect } from 'react';
import Sidebar from './content/SideNav/Sidebar';
import Principal from './content/Principal';
import Caudal from './content/content-pr/Caudal';
import Programas from './content/Programas';
import Moment from 'moment';

import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

var lista = [{name: "Prog1", ini: "10:50"}, {name: "Prog2", ini: "12:50"}];
var routes = [
  {
    path: "/Estados",
    exact: true,
    sidebar: () => <div>Estados</div>,
    main: () => <Principal></Principal>
  },
  {
    path: "/Caudal",
    sidebar: () => <div>Caudal</div>,
    main: () => <Caudal/>
  }
];



lista.map(ele => routes.push({path: "/" + ele.name, exact:true, sidebar: () => <div>{ele.name}</div>, main:() => <Programas ID={ele.name}></Programas>}))

function App() {
  
  const [date, setDate] = useState(1);

    useEffect(() => {
        setInterval(() => {
        setDate(Moment( Date.now()).format('HH:mm:ss  DD MMM'));
        })
    })

  return (
    <Router>
      <main> 
        <Sidebar programas = {lista} sectores = {[]} routes = {routes}></Sidebar>
        <div className="nav-horizontal">
          <button class="display_time">{date}</button>
        <Switch>
            {routes.map((route, index) => (
              <Route
                key={index}
                path={route.path}
                exact={route.exact}
                children={<route.main />}
              />
            ))}
          </Switch>
          </div>
      </main>
    </Router>
  );
}

export default App;
