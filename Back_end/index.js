const express       = require('express');
const morgan        = require('morgan');
const cors          = require('cors');
const app           = express();
const ciclo         = require('./Controllers/ciclo.controllers');
//const {ciclosensor}   = require('./Controllers/sensor.controllers');
const t            = require('./Controllers/sensorExt.controllers');

//Configuracion
app.set('port', process.env.PORT || 3000);

/* Middleawares Funciones de procesamiento de datos
  -Gestiona las peticiones de datos.
*/


app.use(morgan('dev'));
app.use(express.json());    // Todos los datos pasan al modelo json
//app.use(cors({origin: 'http://ec2-52-56-36-159.eu-west-2.compute.amazonaws.com:4200'}));
//app.use(cors({origin: 'http://25.46.102.167:4200'}));
app.use(cors({origin: 'http://185.135.119.254:4200'}));
//app.use(cors({origin: 'http://192.168.18.16:4200'}));
// Routes Sistema de enrutamiento del servidor
app.use(require('./routes/programa.routes'));

// Starrtinf the server

app.listen(app.get('port'), () => {
  console.log('Server on port', app.get('port'));
});


setInterval(ciclo, 1000);
//setInterval(ciclosensor,10);




