const express               = require('express');
const router                = express.Router();
const ControladorProgramas  = require('../Controllers/programa.controllers');



router.get('/', ControladorProgramas.getPrograma);                // Quiero los datos de los programas
router.post('/get', ControladorProgramas.getPrograma_ONE);                // Quiero los datos de los programas
router.post('/edit', ControladorProgramas.editProgram);                     // Editar Programas
router.get('/info', ControladorProgramas.getinfo);                // Quiero los datos de los programas
router.post('/newPrograma',ControladorProgramas.crearPrograma);   // Quiero meter un nuevo programa
router.post('/addCaudalimetro',ControladorProgramas.addCaudalimetro);
router.post('/addPresion',ControladorProgramas.addPresion);
router.get('/Secuencia/:_id', ControladorProgramas.getSecuencias);
router.get('/programas_activos', ControladorProgramas.getProgramasActivos);
router.get('/getcaudal',ControladorProgramas.getcaudal);
router.get('/getCaudalConfig',ControladorProgramas.getCaudalConfig);
router.get('/getPresionConfig',ControladorProgramas.getPresionConfig);
router.get('/getAllAnalogic', ControladorProgramas.getAllAnalogic);
router.post('/getDatos',ControladorProgramas.getDatos);
router.post('/deleteProgram', ControladorProgramas.deleteProgram);
router.post('/getDatosMonth', ControladorProgramas.getDatosMensual);
router.get('/AEMET', ControladorProgramas.getAEMET);
module.exports = router;
