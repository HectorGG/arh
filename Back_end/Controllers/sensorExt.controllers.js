/*
    [+] SISTEMA DE CAPATACION DE LA TELEMETRIA EMITIDA DE FORMA INALAMBRICA.

*/
const nrf24         = require("nrf24");
const {addDatos,getDatoslast}    = require("./../models/Sensor");
var value_nrf24     = new nrf24.nRF24(22,0);
var conectado       = false;
var ValoresSondas   = [];

/*

  PALevel -> Nivel de potencia.
  DataRate -> Cantidad de bits por segundo.

*/
value_nrf24.config({PALevel:nrf24.RF24_PA_MAX,
  EnableLna: false,
  DataRate:nrf24.RF24_1MBPS,
  Channel:76,
  AutoAck:true,
  Irq: nrf24.IRQ
});

/*
    [+] Configuracion y arranque.
*/

value_nrf24.begin();

/*
  Inserta un puerto de Escucha.
  Evalua el estado de la conexion.
*/
var pipenr = value_nrf24.addReadPipe("0x65646f4e31",true);

if(pipenr==-1) console.log("Error!!!");


conectado =  value_nrf24.present();


console.log("ver estado" + conectado);

value_nrf24.read((data,n) => {
  ValoresSondas = [];
  var index = 0;
    if(n > 0){
      data.forEach(ele => {
        var fila = ele.data.toString('utf8').replace(/\0/g, '').split(',');
        console.log(fila);
        addDatos(index, fila[0], fila[1], fila[2], fila[3]); 
        index++;
      });
    }
},function(isStopped,by_user,error_count) {
  console.log("codigo error " + error_count);
  console.log("Por usuario " + by_user);
  console.log("Parada " + isStopped);
});

module.exports.conectado = function conectado() {return conectado;}
//module.exports.GetValoresSondas = function GetValoresSondas() {return getDatoslast(;}
