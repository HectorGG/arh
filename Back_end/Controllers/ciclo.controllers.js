const {getProgramas, addProgram}  = require('../models/Programas');
const {addProgramaActivo, getProgramasActivos, deleteProgramaActivo, 
    getProgramasActivosAll, updateEstado, updateEstado_Abonos, update_Orden_estado,
    updateTrasnscurro_Abonos,  updateTrasnscurro }                                          = require('../models/Programas.Activos');
const {}                                                                                    = require('../models/Configuracion');

const {activar_varvula,desactivar_varvulas, salida}                                         = require('../Controllers/actuadores.controller');
//const {optenerCaudales,optenerSonda}                                                        = require('../Controllers/sensor.controllers');

const { GetAEMET } = require('../Controllers/AEMET.controller');

/**
 * [+] Ver el registro de programas existentes.
 * [+] Insertar un programa activo si este cumple las condiciones.
 * [+] Para aquellos programas activos que ya esten se le aplica un ciclo a sus secuencias.
 */

async function ciclo() {
    
    const ListaProgramasActuales = await getProgramas();

    const timeActual = {
        Hora: new Date().getHours(), 
        Minutos: new Date().getMinutes(), 
        Dia: new Date().getDay()
    };
    
    for(let programa of ListaProgramasActuales){
        const timePrograma                          = {Hora: new Date(programa.inicio).getHours(), Minutos: new Date(programa.inicio).getMinutes()};
        const tiempoFinal                           = calculoTiempoFinal(programa);
        const fechaFinal                            = calculoFechaFinal(timeActual,tiempoFinal);
        const programas_activos                     = await getProgramasActivos(programa.id);
        const contadores_Programas                  = programas_activos.count;

        
        if(timeActual.Hora == timePrograma.Hora && timeActual.Minutos == timePrograma.Minutos && contadores_Programas == 0 && programa.Dias.includes(timeActual.Dia)){
            await addProgramaActivo(activarPrograma(programa,fechaFinal));
        }
    }
    
    const programas_activos = await getProgramasActivosAll();
    for(let S_A of programas_activos){
        await ciclo_secuencia(S_A);
    }
}

/**
 * [+] Permite calcular cuanto timpo requiere el programa.
 * @param {Programa } prog 
 */
function calculoTiempoFinal(prog){
    var calculoFinal = 0;
    for(let sec of prog.Secuencias)
        calculoFinal +=  sec.duracion + sec.preriego;

    return calculoFinal;
}

/**
 * [+] Permite determinar la fecha de finalizacion del programa.
 * @param {*} FInicial 
 * @param {*} contador 
 */
function calculoFechaFinal(FInicial,contador){
    var hora           = FInicial.Hora;
    var minutos        = FInicial.Minutos;
    var tiempoInicio   = hora * 60    + minutos;
    var tiempoTotal    = tiempoInicio + contador;
    var horasFinal     = Math.trunc(tiempoTotal / 60);
    var minutosFinal   = Math.trunc(tiempoTotal % 60);

    if(horasFinal >= 24) horasFinal = Math.trunc(horasFinal - 24);

    const fecha ={Hora: horasFinal, Minutos: minutosFinal};
    return fecha;
}

/**
 * [+] Aplica un ciclo en el abono activo con el fin de incremetar su duracion, si este llega su fin desactivar las varvulas asociados.
 * @param { Lista de abonos activos que estan presentes } A_A 
 */
async function ciclo_abono(A_A){
    A_A.forEach(abono => {
        if(new Date().getHours() == new Date(abono.Inicio).getHours() && new Date().getMinutes() == new Date(abono.Inicio).getMinutes() && abono.estado == 0){
            updateEstado_Abonos(abono.idA);
            activar_varvula(abono.varvulas);
        }
        else if(abono.estado == 1 && new Date() >= new Date(abono.Final)){
            desactivar_varvulas(abono.varvulas);
        }else if(abono.estado == 1)
            updateTrasnscurro_Abonos(abono.idA);
        console.log(abono);
    });

}

/**
 * [+] Aplica un cliclo de duracion a las secuencias activas en ese momento.
 * @param {Lista de secuencias activas } S_A 
 */
async function ciclo_secuencia(S_A){
    if(S_A.orden_secuencia < S_A.Secuencias_Activos.length){
        
        await updateTrasnscurro(S_A.Secuencias_Activos[S_A.orden_secuencia].idS);
       
        if(S_A.Secuencias_Activos[S_A.orden_secuencia].estado == 0) {
            await updateEstado(S_A.Secuencias_Activos[S_A.orden_secuencia].idS);
            activar_varvula(S_A.Secuencias_Activos[S_A.orden_secuencia].varvulas);
        }

        else if (S_A.Secuencias_Activos[S_A.orden_secuencia].estado == 1){
            var A_A = S_A.Secuencias_Activos[S_A.orden_secuencia].Abonos_Activos;
            await ciclo_abono(A_A);
        }

        if(new Date() >= new Date(S_A.Secuencias_Activos[S_A.orden_secuencia].Final)){
            desactivar_varvulas(S_A.Secuencias_Activos[S_A.orden_secuencia].varvulas);
            await updateEstado(S_A.Secuencias_Activos[S_A.orden_secuencia].idS);
            await update_Orden_estado(S_A.id);
           
        }
    }
    else{
        await deleteProgramaActivo(S_A.id);
    }
}

/**
 * [+] Pasa un programa al modo activo. 
 * @param {Programa que se desea activar } programas 
 * @param {Fecha de finalizacion del programa } dateFinal 
 */
function activarPrograma(programas, dateFinal){
    var ListaSecuencia          = [];                                  
    var Ini                     = new Date(programas.inicio);
    var Final                   = new Date();
    var ListaAbonos             = [];
    var ListaSecuencia          = [];

    Final.setHours(dateFinal.Hora);
    Final.setMinutes(dateFinal.Minutos);
    

    programas.Secuencias.forEach(ele_secuencia => {
        ListaAbonos             = [];
        var Inicio              = new Date();
        var Fecha_Calculado     = calculoFechaFinal({Hora:  Ini.getHours(), Minutos: Ini.getMinutes()}, ele_secuencia.preriego);
        Inicio.setHours(Fecha_Calculado.Hora);
        Inicio.setMinutes(Fecha_Calculado.Minutos);
        
        
    
        ele_secuencia.Abonos.forEach(ele_abonos => {
            var Final           = new Date();
            Fecha_Calculado     = calculoFechaFinal({Hora: Inicio.getHours(), Minutos: Inicio.getMinutes()}, ele_abonos.duracion);
            Final.setHours(Fecha_Calculado.Hora);
            Final.setMinutes(Fecha_Calculado.Minutos);

            const abonos = nuevo_abono_activo(Inicio, Final, ele_abonos);
            ListaAbonos.push(abonos);
            
        });

        var FinalSecuencia = new Date();
        var resultado = calculoFechaFinal({Hora: Ini.getHours(), Minutos:Ini.getMinutes()}, ele_secuencia.duracion + ele_secuencia.preriego);
        FinalSecuencia.setHours(resultado.Hora);
        FinalSecuencia.setMinutes(resultado.Minutos);

        const secuencia = nuevo_secuencia_activo(Ini, FinalSecuencia, ele_secuencia,ListaAbonos,ListaSecuencia);
        Ini = FinalSecuencia;
        ListaSecuencia.push(secuencia);

    });

    const progrmas_activo = nuevo_programa_activo(ListaSecuencia, programas, Final);
     
   return progrmas_activo;

}

/**
 *  Crea un nuevo programa activo.
 */
function nuevo_programa_activo(sec_activo, Programa, Final){
    return {
        Secuencias_Activos         : sec_activo,
        ID_Programa                : Programa.id,
        Inicio                     : new Date(Programa.inicio),
        Final                      : Final,
        orden_secuencia            : 0,
        Trascurrido                : 0     
    };
}

/**
 * Crea una secuencia activa.
 */
function nuevo_secuencia_activo(Ini, FinalSecuencia, ele_secuencia, ListaAbonos, ListaSecuencia){
    return {
        Inicio             : Ini,
        Trascurrido        : 0,
        Final              : FinalSecuencia,
        varvulas           : ele_secuencia.varvulas,
        CaudalFinal        : 0,
        DCaudal            : ele_secuencia.PorCaudal,
        estado             : 0,
        Abonos_Activos     : ListaAbonos,
        orden              : ListaSecuencia.length
    }
}

/**
 * Crear una abono activo.
 */
function nuevo_abono_activo(Inicio, Final, ele_abonos){
    return {
        Inicio          : Inicio,
        Trascurrido     : 0,
        Final           : Final,
        varvulas        : ele_abonos.varvulas,
        estado          : 0
    }
}

module.exports = ciclo;
