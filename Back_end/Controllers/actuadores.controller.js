/*
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
*                           SISTEMA DE EJECUCION                          *  
*                                                                         * 
*                                                                         * 
*                                                                         * 
*                                                                         * 
*                                                                         * 
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
*/

/*
    [+] Libreria para la conexion con arduino.

*/ 

var five = require("johnny-five");
var board = new five.Board({ repl: false,port:'/dev/ttyACM0'});

/*
    [+] Lista de objetos pines.
*/

var VARVULAS_ON = [{}];
var PIN = [];
var sensors = [];
var SensorAngData = [0,0,0,0,0,0,0,0,0,0];

/*
    [+] Preparacion de los pines de entradas y salidas.
*/
board.on("ready", ()=> {  
    for(let i = 20; i < 50; i++) PIN.push(new five.Pin({pin:i, type:"digital", mode: five.Pin.OUTPUT}));
    for(let i = 0; i < 10; i++) sensors.push(new five.Sensor("A" + i).on("change", (data)=>{ SensorAngData[i] = data;} ));
    
});
  
/*
[+] Conjunto de funciones que permite la extracion de los datos de los sensores.



*/

/* Salida de todos los sensores analogicos en Amp */

module.exports.getAllAnalogic =  function getAllAnalogic(){
    return SensorAngData;
}
  


/*
    [+] Funciones
    [1] Funcion que permite la activacion de un conjunto de varvulas.
    [2] Funcion que permite la desactivacion de un conjunot de varvulas.

*/

module.exports.activar_varvula = function activar_varvula(pins){

    pins.forEach(ele => {
        var IndexPos = VARVULAS_ON.findIndex(e => e.varvulas == ele);
        if(IndexPos != -1) VARVULAS_ON[IndexPos].cont += 1;
        else 
        {
            VARVULAS_ON.push({varvulas: ele, cont: 1});

            PIN[IndexPos].high();
        }
    });
}

module.exports.desactivar_varvulas = function desactivar_varvulas(pins){
    pins.forEach(ele => {
        var IndexPos = VARVULAS_ON.findIndex(e => e.varvulas == ele);

        if(IndexPos != -1) 
        {
            VARVULAS_ON[IndexPos].cont -= 1;
                if(VARVULAS_ON[IndexPos].cont == 0)
                {

                    VARVULAS_ON.splice(IndexPos,1);
                    PIN[IndexPos].low();
                }
        }

    });

    
}

