const {addProgram, getProgramas, deleteProgram, getProgramas_ONE, editProgram}                                                                      = require('../models/Programas');
const {getProgramasActivosAll}                                                                        = require('../models/Programas.Activos');
const programaCtrl    = {};
//const {optenerCaudales, getAllAnalogic}                                                             = require('../Controllers/sensor.controllers');
const {GetValoresSondas}                                                                              = require('../Controllers/sensorExt.controllers');
const {addConfigSensor_Cau, addConfigSensor_Pre, getConfigSensor_Cau, getConfigSensor_Pre}            = require('../models/Configuracion');
const {getAllAnalogic}                                                                                = require('../Controllers/actuadores.controller');
const {getDatos,getDatoslast, getDatosMonth} = require('../models/Sensor');
const { GetAEMET } = require("./AEMET.controller");

/**
 * Optener todos lo programas guardados.
 */
programaCtrl.getPrograma = async (req,res) => {
  console.log(await getProgramas());
  res.json(await getProgramas());
}

programaCtrl.getPrograma_ONE = async (req,res) => {
  res.json(await getProgramas_ONE(req.body.id));
}

/**
 * Insertar un nuevo programas. 
 */
programaCtrl.crearPrograma = async (req,res) => {
  res.json(await addProgram(req.body));
}

/**
 * Editar un  programa. 
 */
programaCtrl.editProgram = async (req,res) => {
  res.json(await editProgram(req.body));
}


/**
 * Inserta la info de un caudalimetro.
 */
programaCtrl.addCaudalimetro = async (req,res) => {
  res.json(await addConfigSensor_Cau(req.body));
}

/**
 * Insertar la calibracion del  sensor de presion.
 */
programaCtrl.addPresion = async (req,res) => {
  res.json(await addConfigSensor_Pre(req.body));
}

/**
 * Optener la informacion de una secuencia determinada por el parametro id.
 */
programaCtrl.getSecuencias = async (req,res) => {
  const Sec = await DATA[0].findById( new String(req.params._id));
  res.json(Sec.secuencia);
}

/**
 * Optener los valores de todos los sensores de presion.
 */
programaCtrl.getinfo = async (req,res) => {
  res.json(await getDatoslast());
}


programaCtrl.getcaudal = async (req,res) => {

  //res.json(optenerCaudales());
}

programaCtrl.deleteProgram = async(req,res) =>{
  await deleteProgram(req.body.id);
  res.json({"mess":true});
}

/**
 * Optener datos 
 */
programaCtrl.getDatos = async (req,res) => {
  res.json(await getDatos(req.body.id));
}

/**
 * Optiene las medias, maximas y minimas de un mes.
 * @param {*} req contiene el mes de la busqueda. 
 * @param {*} res devuelve los datos solicitados.
 */
programaCtrl.getDatosMensual = async (req,res) =>{
  res.json(await getDatosMonth(req.body.mes,req.body.year));
}

programaCtrl.getDatosWeek = async (req,res) => {
  res.json();
}

/**
 * Optener todos los programas que esten activos.
 */
programaCtrl.getProgramasActivos = async (req,res) => {
  res.json(await getProgramasActivosAll());
}

/**
 * Optener las configuraciones de los caudales.
 */
programaCtrl.getCaudalConfig = async (req, res) => {
  res.json(await getConfigSensor_Cau());
}

/**
 * Optener las configuraciones de las presiones.
 */
programaCtrl.getPresionConfig = async (req, res) => {
  res.json(await getConfigSensor_Pre());
}

/**
 * Optener la info de las entradas analogicas.
 */
programaCtrl.getAllAnalogic = async (req, res) => {
  res.json(await getAllAnalogic());
}

programaCtrl.getAEMET = async (req, res) => {
  res.json( await GetAEMET());
}

module.exports = programaCtrl;
