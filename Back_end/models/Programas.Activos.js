const sequelize = require('./Base.Datos');
const Sequelize = require('sequelize');

const Abonos_Activos = sequelize.define('Abonos_Activos',{
    idA: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      primaryKey: true
    }, 
    Inicio             : Sequelize.DATE,
    Trascurrido        : Sequelize.INTEGER,
    Final              : Sequelize.DATE,
    varvulas           : Sequelize.ARRAY(Sequelize.INTEGER),
    estado             : Sequelize.INTEGER
  });
  
  const Secuencias_Activos = sequelize.define('Secuencias_Activos',{
    idS: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    Inicio             : Sequelize.DATE,
    Trascurrido        : Sequelize.INTEGER,
    Final              : Sequelize.DATE,
    CaudalFinal        : Sequelize.DOUBLE,
    DCaudal            : Sequelize.BOOLEAN,
    varvulas           : Sequelize.ARRAY(Sequelize.INTEGER),
    estado             : Sequelize.INTEGER,
    orden              : Sequelize.INTEGER
  
  });
  
  const Programas_Activos = sequelize.define('Programas_Activos',{
    id: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    ID_Programa        : Sequelize.INTEGER,
    Inicio             : Sequelize.DATE,
    Trascurrido        : Sequelize.INTEGER,
    Final              : Sequelize.DATE,
    orden_secuencia    : Sequelize.INTEGER
  });
  
  

  Programas_Activos.Secuencias_Activos = Programas_Activos.hasMany(Secuencias_Activos, { onDelete: 'cascade' });
  Secuencias_Activos.Abonos_Activos = Secuencias_Activos.hasMany(Abonos_Activos, { onDelete: 'cascade' });
  
  Abonos_Activos.sync();
  Secuencias_Activos.sync();
  Programas_Activos.sync();

  module.exports.addProgramaActivo = function addProgramaActivo(req){
    Programas_Activos.create({
      ID_Programa        : req.ID_Programa,
      Inicio             : req.Inicio,
      Final              : req.Final,
      orden_secuencia    : req.orden_secuencia,
      Trascurrido        : req.Trascurrido,
      Secuencias_Activos : req.Secuencias_Activos  
    },{
      include: [{
        association: Programas_Activos.Secuencias_Activos,
        include: [Secuencias_Activos.Abonos_Activos]
      }],order: [ [ { model: Secuencias_Activos}, 'orden' ] ]
    });
  }

  module.exports.getProgramasActivos = function getProgramasActivos(ids){
     return Programas_Activos.findAndCountAll(
      {
        where: {  
          ID_Programa: ids
       }
     }
    );
  }
  
  module.exports.getProgramasActivosAll = async function  getProgramasActivosAll(){
    return Programas_Activos.findAll(
      {
       include: [{
         model: Secuencias_Activos,
         order: Secuencias_Activos.orden,
         required: true,
         include: [{
           model: Abonos_Activos,
           required: false,
         }],
       }],order: [ [ { model: Secuencias_Activos}, 'orden' ] ]
     }
   );
  }

  module.exports.updateTrasnscurro = function updateTrasnscurro(id){
    Secuencias_Activos.findById(id).then(P_A => {
      P_A.increment({'Trascurrido' : 1} )
    });
  }
  
  module.exports.updateTrasnscurro_Abonos = function updateTrasnscurroAbonos(id){
    Abonos_Activos.findById(id).then(P_A => {
      P_A.increment({'Trascurrido' : 1} )
    });
  }
  
  module.exports.updateEstado = function updateEstado(id){
    Secuencias_Activos.findById(id).then(P_A => {
      P_A.increment({'estado' : 1} )
    });
  }
  
  module.exports.updateEstado_Abonos = function updateEstado_Abonos(id){
    Abonos_Activos.findById(id).then(P_A => {
      P_A.increment({'estado' : 1} )
    });
  }
  module.exports.update_Orden_estado = function update_Orden_estado(id){
    Programas_Activos.findById(id).then(P_A => {
      P_A.increment({'orden_secuencia' : 1} )
    });
  }
  
  module.exports.deleteProgramaActivo = function deleteProgramaActivo(id){
    Programas_Activos.findById(id).then(the_model => {
      the_model.destroy({ force: true })
    });
  }