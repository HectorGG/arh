/* Modelo en la base de datos que se encargara de almacenar la configuracion de los sensores, actuadores y de mas */

const Sequelize = require('sequelize');
const sequelize = require('./Base.Datos');


/* Configuracion de un caudalimetro */
const Caudalimetros = sequelize.define('Confi_Caudalimetro',{
    Factor_K: Sequelize.INTEGER,
    NT_Litro: Sequelize.INTEGER,
    PIN: Sequelize.INTEGER,
    Salida : Sequelize.INTEGER,
    C_TOTAL: Sequelize.BOOLEAN

});

/* Configuracion de presion */
const Presion = sequelize.define('Presion', {
    PIN: Sequelize.INTEGER,
    V_AMP_MIN  :  Sequelize.DOUBLE,
    V_REF_MIN  :  Sequelize.DOUBLE,
    V_AMP_MAX  :  sequelize.DOUBLE,
    V_REF_MAX :   Sequelize.DOUBLE

});

ConfigSensores.belongsTo(Caudalimetros, { onDelete: 'cascade' });
ConfigSensores.belongsTo(Presion, { onDelete: 'cascade' });
 
