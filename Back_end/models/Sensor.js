const Sequelize = require('sequelize');
const sequelize = require('./Base.Datos');
const Op = Sequelize.Op;

const data = sequelize.define('data',{
    idData: {
      type: Sequelize.DATE,
      primaryKey:true
    },
    idSonda:{
      type: Sequelize.INTEGER,
    },
    temperatura: {
      type: Sequelize.FLOAT
    },
    humedad:{
      type: Sequelize.FLOAT
    },
    humedadT:{
      type: Sequelize.FLOAT
    },
    VCC:{
      type: Sequelize.FLOAT
    }
  });

  data.sync();

module.exports.addDatos = function addDatos(idS, hum, temp, ht, vcc){
    data.create({
        idData          : Date.now(),
        idSonda         : idS,
        temperatura     : temp,
        humedad         : hum,
        humedadT        : ht,
        VCC             : vcc,
    })
}

module.exports.getDatos = function getDatos(ids){
    return data.findAll({
      order: [['createdAt','DESC']],
        where: {  
        idSonda: ids,
        idData: {
          [Sequelize.Op.gt]: new Date().getTime() - 24 * 60 * 60 * 1000,
        }
      }, 
    });
}


module.exports.getDatosMonth = async function getDatosMonth(mes, year){

  const value = await data.findAll({
    attributes: [
      [Sequelize.literal(`DATE("createdAt")`), 'date'],
      [Sequelize.literal(`COUNT(*)`), 'count'],
      [Sequelize.literal('SUM(temperatura)/COUNT(*)'), 'temp'],
      [Sequelize.literal('SUM(humedad)/COUNT(*)'), 'hum'],
      [Sequelize.literal('SUM("humedadT")/COUNT(*)'), 'humt'],
      [Sequelize.literal('MIN(temperatura)'), 'tempmin'],
      [Sequelize.literal('MIN(humedad)'), 'hummin'],
      [Sequelize.literal('MIN("humedadT")'), 'humtmin'],
      [Sequelize.literal('MAX(temperatura)'), 'tempmax'],
      [Sequelize.literal('MAX(humedad)'), 'hummax'],
      [Sequelize.literal('MAX("humedadT")'), 'humtmax']
    
  ],
  group: ['date'],
  where: sequelize.and(sequelize.where(sequelize.fn('date_part', 'Mon', sequelize.col('createdAt')),mes),
  sequelize.where(sequelize.fn('date_part', 'Year', sequelize.col('createdAt')),year)),
  
 


  });

  return value;


}





module.exports.getDatosWeek = async function getDatosWeek(){
  
  
}

module.exports.getDatoslast = function getDatoslast(){
  return data.findAll({
    limit: 1,
    order: [['createdAt','DESC']],
  });
}