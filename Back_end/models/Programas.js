const Sequelize = require('sequelize');
const sequelize = require('./Base.Datos');
const uuidv4    = require('uuidv4');

const Abonos = sequelize.define('Abonos',{
  idA: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    primaryKey: true
  }, 
  duracion:  Sequelize.INTEGER,
  varvulas : Sequelize.ARRAY(Sequelize.INTEGER)
});

const Secuencias = sequelize.define('Secuencias',{
  idS: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  preriego :  Sequelize.DOUBLE,
  varvulas :  Sequelize.ARRAY(Sequelize.INTEGER), 
  duracion :  Sequelize.INTEGER,
  DCaudal  :  Sequelize.DOUBLE,
  PorCaudal:  Sequelize.BOOLEAN,
  orden    :  Sequelize.INTEGER
});


const Programa = sequelize.define('Programa',{
  id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  Dias : Sequelize.ARRAY(Sequelize.INTEGER),
  inicio : Sequelize.DATE
});
  
  Programa.Secuencias = Programa.hasMany (Secuencias, { onDelete: 'cascade' });
  Secuencias.Abonos = Secuencias.hasMany (Abonos, { onDelete: 'cascade' });
  
  Abonos.sync();
  Secuencias.sync();
  Programa.sync();



  /* Definicion de los metodos de los diferentes modelos */
  
  /* Funcion que devuelve todos los programas de la base de datos */
  module.exports.getProgramas = function getProgramas(){
    return Programa.findAll({include: [{
      model: Secuencias,
      required: true,
      include: [{
        model: Abonos,
        required: false
      }],
      
    }],order: [ [ { model: Secuencias}, 'orden' ] ]});
  }

  module.exports.getProgramas_ONE = function getProgramas_ONE(req){
    return Programa.findAll({include: [{
      model: Secuencias,
      required: true,
      include: [{
        model: Abonos,
        required: false
      }],
      
    }],order: [ [ { model: Secuencias}, 'orden' ] ],
    where:{
      id: req
    }
    });
  }
  
  module.exports.addProgram =  function insertarPrograma(req){
    Programa.create({
      inicio              : req.inicio,
      Dias                : req.Dias,
      Secuencias          : req.Secuencias
    },
    {include: [{
      association: Programa.Secuencias, 
      include: [ Secuencias.Abonos]
    }]});
  }

  module.exports.editProgram = function editProgram(req){
    console.log(req);
    Programa.update({
      inicio              : req.inicio,
      Dias                : req.Dias,
      Secuencias          : req.Secuencias,
     
    },
    { where  : {id: req.id }   },
    {include: [{
      association: Programa.Secuencias, 
      include: [ Secuencias.Abonos]
    }]},
   );
  }


  module.exports.deleteProgram = function deleteProgram(idP){
    Programa.destroy({
        where:{
          id: idP,
        }
    });
  }
  
  
  