/* Modelo en la base de datos que se encargara de almacenar la configuracion de los sensores, actuadores y de mas */

const Sequelize = require('sequelize');
const sequelize = require('./Base.Datos');


/* Configuracion de un caudalimetro */
const Caudalimetros = sequelize.define('Confi_Caudalimetro',{
    Nombre  : Sequelize.STRING,
    Factor_K: Sequelize.INTEGER,
    NT_Litro: Sequelize.INTEGER,
    PIN: Sequelize.INTEGER,
    Salida : Sequelize.INTEGER,
    C_TOTAL: Sequelize.BOOLEAN

});

/* Configuracion de presion */
const Presion = sequelize.define('Presion', {
    Nombre  : Sequelize.STRING,
    PIN: Sequelize.INTEGER,
    V_AMP_MIN  :  Sequelize.DOUBLE,
    V_REF_MIN  :  Sequelize.DOUBLE,
    V_AMP_MAX  :  Sequelize.DOUBLE,
    V_REF_MAX  :  Sequelize.DOUBLE

});


Presion.sync();
Caudalimetros.sync();

module.exports.addConfigSensor_Cau = function addConfigSensor_Cau(req){
   Caudalimetros.create({
       Nombre               : req.Nombre,
       Factor_K             : req.Factor_K,
       NT_Litro             : req.NT_Litro,
       PIN                  : req.PIN,
       Salida               : req.Salida,
       C_TOTAL              : req.C_TOTAL
   })
}

module.exports.addConfigSensor_Pre = function addConfigSensor_Pre(req){
    Presion.create({
        Nombre               : req.Nombre,
        PIN: req.PIN,
        V_AMP_MIN  :  req.V_AMP_MIN,
        V_REF_MIN  :  req.V_REF_MIN,
        V_AMP_MAX  :  req.V_AMP_MAX,
        V_REF_MAX  :  req.V_REF_MAX
    })
}

module.exports.getConfigSensor_Cau = function getConfigSensor_Cau(){
    return Caudalimetros.findAndCountAll();
}

module.exports.getConfigSensor_Pre = function getConfigSensor_Pre(){
    return Presion.findAndCountAll();
}

 
