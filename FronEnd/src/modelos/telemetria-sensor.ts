export class Telemetria_Sensor {
    idDate                 : Date;
    idSonda                : Number;
    temperatura            : Number;
    humedad                : Number;
    humedadT               : Number;
    VCC                    : Number;
   };
 
 export class Caudal {
    id : Number;
    contador : Number;
    contadorTotal : Number;
 
 };