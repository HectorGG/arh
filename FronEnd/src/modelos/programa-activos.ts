export class Abonos_Activos {
    idA                : Number;
    Inicio             : Date;
    Trascurrido        : Number; 
    Final              : Date;
    varvulas           : Number[]; 
    estado             : Number;
  };
  
  export class Secuencias_Activos {
    idS                : Number;
    Inicio             : Date;
    Trascurrido        : Number; 
    Final              : Date;
    CaudalFinal        : Number;
    DCaudal            : Boolean;
    varvulas           : [Number];
    estado             : Number;
    Abonos_Activos             : Abonos_Activos[];
  
  };
  
  export class Programas_Activos {
    id                : Number;
    ID_Programa        : Number;
    Inicio             : Date;
    Trascurrido        : Number;
    Final              : Date;
    orden_secuencia    : Number;
    Secuencias_Activos :  Secuencias_Activos[];
  };