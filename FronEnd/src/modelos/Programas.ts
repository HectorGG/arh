/**
 * @author Hector Gonzalez Guerreiro.
 * @version 2.0.0
 * 
 * Indica que tipo de abono y la duracion del mismo.
 * 
 * @param ida Identificador unico.
 * @param varvulas Lista de varvulas que activar cuando se inicie el abonado.
 * @param duracion Tiempo en minutos que dura el abonado.
 * 
 */

export class Abonos {
    id_a     : number;
    varvulas : number[];
    duracion : number;

    constructor(id_a:number, v:number[], duracion:number){
        this.id_a = id_a;
        this.varvulas = v;
        this.duracion = duracion;
    }
}

/** Una secuencia viene marcada por un periodo de tiempo concreto dentro de un programa, dentro del mismo
 * tenemos un periodo de prerriego que solo administra agua, un conjunto de varvulas que debe abrir una duracion total del mismo
 * o un caudal maximo y por ultimo un conjunto de abonos que debe activar segun convenga.
 * 
 * @param id identificador unico para cada secuencia.
 * @param preriego  Duracion del mismo.
 * @param varvulas  lista de entero, cada entero representa el id de la varvula que hay que activar.
 * @param duracion establece la duracion del periodo de riego despues del preriego, en el cual tambien 
 * se activa el abono.
 * @param Abonos lista de abonos que se activaran en el periodo despues del pre riego.
 * @param DCaudal caudal maximo que emitira esta secuencia, sustituye la funcion de duracion.
 * @param PorCaudal indica cual de los dos modos se utiliza para definir la finalizaciond e una secuencia.
 * mediante cuadal o duracion de tiempo.
 * @param orden indica cual es el orden de ejecucion.
 * */
export class secuencias {
    id       : number;
    preriego : number;
    varvulas : number[];
    duracion : number;
    Abonos   : Abonos[ ];
    DCaudal  : number;
    PorCaudal: boolean;
    orden    : number;

    constructor(id:number,pre:number, v:number[], duracion:number, PorCaudal:boolean, abo: Abonos[], orden:number){
        this.preriego = pre;
        this.varvulas = v;
        this.duracion =duracion;
        this.DCaudal = duracion;
        this.PorCaudal = PorCaudal;
        this.Abonos = abo;
        this.id = id;
        this.orden = orden;
    }
}

/**
 * @author Hector Gonzalez Guerreiro.
 * @version 2.0.0
 * Definie la clase objeto Programa.
 * @param inicio define en que hora del dia se inicia el programa en formato 24H.
 * @param dias  lista que representa los dias de la semana en los cuales el programa se activa.
 *  si no se encuentra no se activa para ese dia.
 * @param secuencias conjunto de secuencias que posee un programa.
 * 
 * 
 * */
export class programas  {
    inicio  : Date;
    dia     : number[];
    Secuencias     : secuencias[ ];

    constructor(inicio:Date, dia:number[], sec: secuencias[ ]){
        this.dia = dia;
        this.inicio = inicio;
        this.Secuencias = sec;
    }

}
