export class PresionConfig {
    Nombre: string;
    Factor_K: number;
    NT_Litro: number;
    PIN: number;
    Salida :number;
    C_TOTAL: number;

    constructor(){
        this.Nombre = "";
        
    }

}

export class CaudalimetroConfig {
    Nombre: string;
    PIN: number;
    V_AMP_MIN  : number;
    V_REF_MIN  : number;
    V_AMP_MAX  : number;
    V_REF_MAX  : number;

    constructor(){
        this.Nombre = "";
        
    }
}