export class Telemetria_Sensor_G {
    idData                 : Date;
    temperatura            : Number;
    humedad                : Number;
    humedadT               : Number;
    VCC                    : Number;
    idSonda                : Number;
    createdAt              : Date;
   };


export class Telemetria_Sensor_Month {
    date                : Date
    hum                 : Number;
    hummax              : Number;
    hummin              : Number;
    humt                : Number;
    humtmax             : Number;
    humtmin             : Number;
    temp                : Number;
    tempmax             : Number;
    tempmin             : Number;

};
export class Telemetria_Sensor_GM {
    lista                   : any;
}

