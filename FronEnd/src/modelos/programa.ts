export class Abonos {
    idA     : number;
    varvulas : number[];
    duracion : number;

    constructor(id_a:number, v:number[], duracion:number){
        this.idA = id_a;
        this.varvulas = v;
        this.duracion = duracion;
    }
}

export class secuencias {
    id      : number;
    preriego : number;
    varvulas : number[];
    duracion : number;
    Abonos   : Abonos[ ];
    DCaudal  : number;
    PorCaudal: boolean;
    orden    : number;

    constructor(id:number,pre:number, v:number[], duracion:number, PorCaudal:boolean, abo: Abonos[], orden:number){
        this.preriego = pre;
        this.varvulas = v;
        this.duracion =duracion;
        this.DCaudal = duracion;
        this.PorCaudal = PorCaudal;
        this.Abonos = abo;
        this.id = id;
        this.orden = orden;
    }
}

export class programas  {
    inicio  : Date;
    Dias     : number[];
    Secuencias     : secuencias[ ];

    constructor(inicio:Date, dia:number[], sec: secuencias[ ]){
        this.Dias = dia;
        this.inicio = inicio;
        this.Secuencias = sec;
    }

}