import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CaudalimetroConfig, PresionConfig}        from '../modelos/config/config';
import { programas }                       from '../modelos/Programas';

@Injectable({
  providedIn: 'root'
})

/**
 *     | Comunicacion |
 * 
 * Establece comunicacion con el servidor 'back end' encargado de preocesar la informacion y el 
 * acceso a la base de datos.
 * 
 * Para ello la clase contendra las funciones necesarias que permita tanto enviar informacion, como solicitarla.
 * @param URL_API Direccion donde se aloja el back end.
 *    
 */

export class ComunicacionService {

  readonly URL_API = 'http://192.168.18.16:3000/';

  
/**
   * 
   * @param http me perimite enviar o recibir a traves del protocolo http informacion del servidor back end. 
   */
  constructor(private http: HttpClient) { }

  /**
   * Devuelve todos los programas almacenados.
   */
  getPrograma(){
    return this.http.get(this.URL_API);
  }
  /**
   * Devuelve el resultado del instituo metereologico.
   */
  getAEMET(){
    return this.http.get(this.URL_API + "AEMET");
  }

  /**
   * Retorna un unico programa.
   * @param id 
   */
  getPrograma_ONE(id: number){
    return this.http.post(this.URL_API +"get",{ 'id':id});
  }


  /**
   * Devuelve todos los programas que estan activos.
   */
  getProgramas_Activos(){
    return this.http.get(this.URL_API + "programas_activos");
  }
  /**
   * Inserta un nuevo programa.
   * @param Prog Programa nuevo.
   */
  addPrograma(Prog:programas){
    return this.http.post(this.URL_API + 'newPrograma', Prog);
  }

  /**
   * Inserta una nueva configuracion para el caudalimetro.
   * @param Cauda Configuracion del caudalimetro.
   */
  addCaudalimetro(Cauda:CaudalimetroConfig){
    return this.http.post(this.URL_API + 'addCaudalimetro', Cauda);
  }

  /**
   * Inserta una nueva configuracion para el sensor de presion.
   * @param Cauda Configuracion del sensor de presion.
   */
  addPresion(Presion:PresionConfig){
    return this.http.post(this.URL_API + 'addPresion',Presion);
  }

  /**
   * Obtener todas las secuencias asociadas a un programa.
   * @param _idPrograma identificador del programa.
   */
  getSecuencias(_idPrograma: number){
    return this.http.get(this.URL_API + "Secuencia" + `/${_idPrograma}`);
  }
  /**
   * 
   */
  getinfo(){
    return this.http.get(this.URL_API + 'info');
  }
  /**
   * Obtiene informacion del caudal.
   */
  getcaudal(){
    return this.http.get(this.URL_API + 'getcaudal');
  }
  /**
   * Optiene informacion del la configuracion actual del caudalimetro.
   */
  getCaudalConfig(){
    return this.http.get(this.URL_API + 'getCaudalConfig');
  }
  /**
   * Optiene informacion del la configuracion actual del sensor de presion.
   */
  getPresionConfig(){
    return this.http.get(this.URL_API + 'getPresionConfig');
  }

  /**
   * retorna los estados de los sensores analogicos del arduino.
   */
  getAllAnalogic(){
    return this.http.get(this.URL_API + 'getAllAnalogic');
  }
  /**
   * 
   * @param id 
   */
  getDataG(id : number){
    return this.http.post(this.URL_API + 'getDatos',{ 'id':id});
  }
  /**
   * Elimina por completo un programa.
   * @param id identificador unico del programa.
   */
  deleteProgram(id: number){
    return this.http.post(this.URL_API + "deleteProgram",{ 'id':id});
  }
  /**
   * Retoma informacion mensual de temperatura y humedad de la sonda.
   * @param mes Mes del año {0...11}
   * @param year Año {2011}
   */
  getDatosMensual(mes: number, year:number){
    return this.http.post(this.URL_API + 'getDatosMonth',{'mes': mes, 'year':year});
  }
}