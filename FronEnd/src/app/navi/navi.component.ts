import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navi',
  templateUrl: './navi.component.html',
  styleUrls: ['./navi.component.css']
})
export class NaviComponent implements OnInit {
  FECHA:Date;

  constructor() {
    setInterval(() => this.tick(),1000);
  }

  private tick(){
    this.FECHA = new Date();
  }


  ngOnInit() {
  }

}
