import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TelemetriaCaudalComponent } from './telemetria-caudal.component';

describe('TelemetriaCaudalComponent', () => {
  let component: TelemetriaCaudalComponent;
  let fixture: ComponentFixture<TelemetriaCaudalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TelemetriaCaudalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TelemetriaCaudalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
