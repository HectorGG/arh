import { Component, OnInit } from '@angular/core';
import { ComunicacionService } from '../../../servicios/comunicacion.service';
import { Telemetria_Sensor, Caudal} from '../../../modelos/Telemetria_Sensor';

@Component({
  selector: 'app-telemetria-caudal',
  templateUrl: './telemetria-caudal.component.html',
  styleUrls: ['./telemetria-caudal.component.css']
})
export class TelemetriaCaudalComponent implements OnInit {
  TabCaudal     : Caudal;


  constructor( private programaServicio: ComunicacionService) {
    setInterval(() => {
    /*this.getCaudal(); */}, 60000);
   }

  getCaudal(){
    this.programaServicio.getcaudal().subscribe(res => {
      this.TabCaudal = res as Caudal;

    });
  }

  ngOnInit() {
    //this.getTelemetria();
    //this.getCaudal();
  }
}
