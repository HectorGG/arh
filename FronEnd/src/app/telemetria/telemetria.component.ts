import { Component, OnInit } from '@angular/core';
import { ComunicacionService } from '../../servicios/comunicacion.service';
import { Caudal, Telemetria_Sensor} from '../../modelos/telemetria-sensor';
@Component({
  selector: 'app-telemetria',
  templateUrl: './telemetria.component.html',
  styleUrls: ['./telemetria.component.css']
})
export class TelemetriaComponent implements OnInit {

  TabSensor     : Telemetria_Sensor;
  TabCaudal     : Caudal;

  constructor( private programaServicio: ComunicacionService) {
    setInterval(()=> {
      this.getTelemetria(); },1000*30);
    /*setInterval(() => {
      this.getCaudal(); }, 60000);*/
   }

  ngOnInit() {
    this.getTelemetria();
    //this.getCaudal();
  }

  getTelemetria(){
    this.programaServicio.getinfo().subscribe(res => {
      this.TabSensor = res as Telemetria_Sensor;
    });
  }

}
