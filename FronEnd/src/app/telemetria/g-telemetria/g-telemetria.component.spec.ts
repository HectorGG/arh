import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GTelemetriaComponent } from './g-telemetria.component';

describe('GTelemetriaComponent', () => {
  let component: GTelemetriaComponent;
  let fixture: ComponentFixture<GTelemetriaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GTelemetriaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GTelemetriaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
