import { Component, OnInit } from '@angular/core';
import { secuencias, Abonos, programas} from "../../modelos/Programas";
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { ComunicacionService } from '../../servicios/comunicacion.service';
import { ActivatedRoute, Router }                       from '@angular/router';
import { Secuencias_Activos } from 'src/modelos/programa-activos';
import { AlertasService} from '../../servicios/alertas.service';

@Component({
  selector: 'app-add-programa',
  templateUrl: './add-programa.component.html',
  styleUrls: ['./add-programa.component.css']
})
export class AddProgramaComponent implements OnInit {
  dropdownSettings:IDropdownSettings = {};
  Secuencias:secuencias[] = [];
  Abonos:Abonos[]         = [];
  MODE:boolean = false;
  PROGRAMAS:programas;
  IDEN:number;

  DiaSemana =  [
    {name:"Lunes",       valor:false, pos: 1},
    {name: "Martes",     valor:false, pos: 2},
    {name: "Miercoles",  valor:false, pos: 3},
    {name: "Jueves",     valor:false, pos: 4},
    {name: "Viernes",    valor:false, pos: 5},
    {name: "Sabado",     valor:false, pos: 6},
    {name: "Domingo",    valor:false, pos: 0}];

  time = "";



  Col_Secuencia = ["ID","Preriego","ListaVarvulas","¿Caudal?","Caudal/Tiempo","Abonos","Acciones"];
  Col_Abonos    = ["ID","Abonadora", "Duracion", "Acciones"];                               
  
  _ProSecuencia = 0;
  _Abonos = [];
  _AbonosSelec = [];
  _Varvulas = [];
  _VarvulasSelec = [];
  _DSecuencia = 0;
  _DC:boolean = true;

  _VarvulasAB = [];
  _VarvulasSelecAB = [];
  _DAbono = 0;


  constructor(private programaServicio: ComunicacionService, private ruta: ActivatedRoute,private rutaM: Router, private Alert: AlertasService) {
    //setTimeout(()=> { this._Abonos = this.Abonos; },1000);
   
  }

  clearSec(){
  this._ProSecuencia = 0;
  this._VarvulasSelec = [];
  this._DSecuencia = 0;
  this._DC = false;

  }

  clearAbo(){
    this._VarvulasAB = [];
    this._VarvulasSelecAB = [];
    this._DAbono = 0;
  }

  ADD_Secuencia(){
    console.log(this.Abonos);
    console.log(this._AbonosSelec);

    var ABO:Abonos[] = [];
    
    this._AbonosSelec.forEach(index => {
      ABO.push(this.Abonos.find(a => a.id_a === index.id));
    })

    console.log( ABO);

    this.Secuencias.push(new secuencias(this.Secuencias.length,this._ProSecuencia,
      this._VarvulasSelec.map(ele => {return ele.id;}),
      this._DSecuencia,
      this._DC,
      ABO,
      this.Secuencias.length));
    
  }

  BorradoSecuencia(item:number){
    this.Secuencias.splice(item,1);
    this.Secuencias.filter(sec => sec.id > item).map(sec => sec.id--);
}

  ADD_Abono(){
    console.log(this._VarvulasSelecAB);
    var listaAUX = [];
    this._VarvulasSelecAB.map(ele => { listaAUX.push(ele.id)});
    this.Abonos.push(new Abonos(this.Abonos.length,listaAUX,this._DAbono));
    this._Abonos = this._Abonos.concat({id : this.Abonos.length - 1, text: 'AB:' + (this.Abonos.length - 1)});
  }

  BorradoAbono(item:number){
    var ListaAux = [];

    this.Abonos.splice(item,1);
    this._Abonos.splice(item,1);
    this._Abonos = this._Abonos.concat();

    
    
    this.Secuencias.filter(ele => ele.Abonos.findIndex(abo => abo.id_a == item) >= 0).map(ele => {
      ele.Abonos.splice(ele.Abonos.findIndex(abo => abo.id_a == item),1);
    });
    
    this.Abonos.filter(abo => abo.id_a > item).map(abo => abo.id_a--);
    this._Abonos.filter(ele => ele.id > item).map(ele => {ele.id --; ele.text =  'AB: ' + ele.id});
    this._Abonos = this._Abonos.concat();
    
  }
  
  addPrograma(){
    var d = new Date();
    var hora = Number (this.time.split(":")[0]);
    var minuto = Number (this.time.split(":")[1]);
    d.setHours(hora);
    d.setMinutes(minuto);
    var aux = [];
    this.DiaSemana.map(ele =>{ if(ele.valor) aux.push(ele.pos);});
    console.log(aux);
    var programa:programas = new  programas(d,aux, this.Secuencias);
    
    if(!this.MODE){
    
      this.programaServicio.addPrograma(programa).subscribe(res => {
        console.log(res);
        this.Alert.success("Nuevo Programa");
      });
    }
    else{
      this.programaServicio.deleteProgram(this.IDEN).subscribe(res => {console.log(res);});
      this.programaServicio.addPrograma(programa).subscribe(res => { console.log(res);  this.Alert.success("Programa Editado");});
      //this.programaServicio.editarProgramas(programa).subscribe(res => this.Alert.success("Programa Editado correctamente"));
    }
    this.rutaM.navigate(['/']);
  }

  ngOnInit(): void {
    this.IDEN = parseInt(this.ruta.snapshot.paramMap.get("id"),10);
    this.MODE = (this.ruta.snapshot.paramMap.get("mode") == 'true'); 
    
    
    for (let index = 0; index < 30; index++) this._Varvulas.push({id: index, text: "V:" + index});
    for (let index = 30; index < 40; index++) this._VarvulasAB.push({id: index, text: "AB:" + index});
    
    if(this.MODE){
      let programa_aux = this.programaServicio.getPrograma_ONE( this.IDEN).subscribe(res => {
        this.PROGRAMAS = res as programas;
        console.log(this.PROGRAMAS);

        
        var SECUENCIA_AUX = this.PROGRAMAS[0].Secuencias as secuencias[];

        SECUENCIA_AUX.map(ele => {
          var ABONO_AUX  = ele.Abonos as Abonos[];
          var ABO:Abonos[] = [];
          ABONO_AUX.map(ele2 => {
            console.log(ele2.id_a);
            
            if(this.Abonos.findIndex(inde => ele2.id_a == inde.id_a) == -1){
              ABO.push(new Abonos(ele2.id_a - 1, ele2.varvulas,ele2.duracion));
              this.Abonos.push(new Abonos(ele2.id_a - 1, ele2.varvulas,ele2.duracion));
              this._Abonos = this._Abonos.concat({id: ele2.id_a - 1, text: "AB:" + (ele2.id_a - 1)});
            }
            
          });

          this.Secuencias.push(new secuencias(this.Secuencias.length, ele.preriego, ele.varvulas,ele.duracion,ele.PorCaudal,ABO,ele.orden));
          

          

        });

        console.log(this.Secuencias);
        

        this.time = new Date(this.PROGRAMAS[0].inicio).getHours() + ":" + new Date(this.PROGRAMAS[0].inicio).getMinutes();

        this.DiaSemana.map(ele => {
          this.PROGRAMAS[0].Dias.map(eleD => {
            if(ele.pos == eleD){
              ele.valor = true;
            }
          })
        })

      });
    }


    this.dropdownSettings = {
      singleSelection: false,
      idField: 'id',
      textField: 'text',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
    }; 
      
  }
  
}
