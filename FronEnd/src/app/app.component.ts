import { Component, OnInit } from '@angular/core';
import { ComunicacionService} from '../servicios/comunicacion.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit  {
  title = 'FronEND';
  url_estandar: string = "../assets/img.clima/svg/"
  FECHA: Date;
  texto: string = "";
  clima: string = "../assets/img.clima/svg/001-cloud.svg";

  constructor(private programaServicio: ComunicacionService){
      setInterval (() => {this.getFecha();},1000);
      setInterval (() => {this.getPA();},1000*60*15);
    
  }

  ngOnInit() {
   this.getPA();
   this.getFecha();
  
  }

  getFecha(){
    this.FECHA = new Date();
  }

  getPA(){
    this.programaServicio.getAEMET().subscribe(ele => {

      console.log(ele['today']['value']);

      switch(ele['today']['value']){
        case '17'  :
        case '11'  : this.clima = this.url_estandar + "005-sun.svg"; break;
        case '17n' :
        case '11n' : this.clima = this.url_estandar + "006-night.svg"; break;
        case '12'  : this.clima = this.url_estandar + "010-cloudy day.svg"; break;
        case '12n' : this.clima = this.url_estandar + "011-cloudy night.svg"; break;
        case '13'  :
        case '13n' : this.clima = this.url_estandar + "001-cloud.svg"; break;
        case '14'  :
        case '14n' : 
        case '15'  :
        case '16n' : this.clima = this.url_estandar + "010-cloudy day.svg"; break;
        case '23'  :
        case '23n' :
        case '24'  :
        case '24n' :
        case '25'  :
        case '25n' :
        case '26'  :
        case '26n' : this.clima = this.url_estandar + "002-rainy.svg"; break;
        case '44'  :
        case '44n' :
        case '43'  :
        case '43n' :
        case '45n' :
        case '46n' : this.clima = this.url_estandar + "021-rain.svg"; break;
        case '51'  :
        case '51n' :
        case '52'  :
        case '52n' :
        case '53'  :
        case '53n' :
        case '54'  :
        case '54n' :
        case '61'  :
        case '61n' :
        case '62'  :
        case '62n' :
        case '63'  :
        case '63n' :
        case '64'  :
        case '64n' : this.clima = this.url_estandar + "013-thunder.svg"; break;
      }

      console.log(ele);

      this.texto = ele['today']['tmp']['current'];
      
    });
  }
}
