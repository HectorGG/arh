import { Component, OnInit } from '@angular/core';
import { ComunicacionService } from '../../servicios/comunicacion.service';
import {  PresionConfig, CaudalimetroConfig } from '../../modelos/config/config';

@Component({
  selector: 'app-configuracion',
  templateUrl: './configuracion.component.html',
  styleUrls: ['./configuracion.component.css']
})
export class ConfiguracionComponent implements OnInit {
  TabCaudal         : CaudalimetroConfig[] = [];
  TabPresion        : PresionConfig[] = [];
  Caudal            : CaudalimetroConfig = new CaudalimetroConfig();
  Presion           : PresionConfig =  new PresionConfig();

  /* Optencion de los valores de todos Amperimetros */

  AMP               :number[] = [];
  

  collectionSize: number[] = [0, 0];
  page : number[] = [0, 0];
  pageSize : number[] = [4, 4];
  currentOrientation = 'horizontal';

  constructor(private programaServicio: ComunicacionService) { }

  addCaudalimetro(){
    this.programaServicio.addCaudalimetro(this.Caudal).subscribe(res => {
      this.ngOnInit();
    });
    
  }

  addPresion(){
    this.programaServicio.addPresion(this.Presion).subscribe(res => {
      this.ngOnInit();
    });
    
  }

  delete(){

  }

  ngOnInit() {
    this.programaServicio.getCaudalConfig().subscribe(res => {
      console.log(res);
      this.TabCaudal = res as CaudalimetroConfig[];
      this.collectionSize[0] = this.TabCaudal.length;
    })

    this.programaServicio.getPresionConfig().subscribe(res => {
      console.log(res);
      this.TabPresion = res as PresionConfig[];
      this.collectionSize[1] = this.TabPresion.length;
    })

    setInterval(()=> {
      this.programaServicio.getAllAnalogic().subscribe(res => {
        this.AMP = res as number[];
      }) },1000); 
    }
    
  

}
