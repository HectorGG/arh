import { Component, OnInit, ViewEncapsulation  } from '@angular/core';
import { programas } from '../../modelos/Programas';
import { ComunicacionService } from '../../servicios/comunicacion.service';

import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-vista-programa',
  templateUrl: './vista-programa.component.html',
  styleUrls: ['./vista-programa.component.css'],
  providers: [NgbModalConfig, NgbModal],
  encapsulation: ViewEncapsulation.None,
  styles: [`
    .dark-modal .modal-content {
      background-color: #292b2c;
      color: white;
    }
    .dark-modal .close {
      color: white;
    }
    .light-blue-backdrop {
      background-color: #5cb3fd;
    }
  `]
})
export class VistaProgramaComponent implements OnInit {
  TabPrograma       : programas[] = [];
  SelectedSecuencia : number = -1;
  SelectedPrograma  : number = -1;
  DiasName : String[] = [ "Domingo","Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabados"];
  ProgramaVisionado: programas;
  collectionSize : number = 0;
  closeResult: string;
  page = 1;
  pageSize = 8;


  constructor(private programaServicio: ComunicacionService, config: NgbModalConfig, private modalService: NgbModal) { 
    config.backdrop = 'static';
    config.keyboard = false;
  }

  progSelecionado(_id){
    if(this.SelectedPrograma != _id){
       this.SelectedPrograma = _id;
       this.SelectedSecuencia = -1;
    }
    else {
      this.SelectedSecuencia = -1;
      this.SelectedPrograma = -1;
    }
  }

  secuenciaSelecionado(_id){
    if(this.SelectedSecuencia != _id ) this.SelectedSecuencia = _id;
    else this.SelectedSecuencia = -1;
  }

  get countries(): programas[] {
    return this.TabPrograma
      .map((country, i) => ({id: i + 1, ...country}))
      .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
  }

  
  open(content,ProActV) {
    this.ProgramaVisionado = ProActV;
    this.modalService.open(content,  { size: 'lg' });
  }

  
  delete(pr){
    console.log(pr.id);
    this.programaServicio.deleteProgram(pr.id).subscribe(res => {
      console.log(res);
      if (res) {
        alert("Eliminado");
        this.modalService.dismissAll();
        this.ngOnInit();
      }
    });
  }
  
  ngOnInit() {
      this.programaServicio.getPrograma().subscribe(res => {
      this.programaServicio.Programas = res as programas[];

      this.TabPrograma =  this.programaServicio.Programas;
      this.collectionSize = this.TabPrograma.length;
    })
  }

}
