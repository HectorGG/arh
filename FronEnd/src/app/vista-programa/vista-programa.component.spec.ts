import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VistaProgramaComponent } from './vista-programa.component';

describe('VistaProgramaComponent', () => {
  let component: VistaProgramaComponent;
  let fixture: ComponentFixture<VistaProgramaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VistaProgramaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VistaProgramaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
