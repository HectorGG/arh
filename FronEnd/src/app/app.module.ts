import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { HttpClientModule, /* other http imports */ } from "@angular/common/http";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { ProgramasActivosComponent } from './programas-activos/programas-activos.component';
import { ProgramasComponent } from './programas/programas.component';
import { TelemetriaComponent } from './telemetria/telemetria.component';
import { FormsModule } from '@angular/forms';
import { PrincipalComponent } from './principal/principal.component';
import { AddProgramaComponent } from './add-programa/add-programa.component';

import { NgbPaginationModule, NgbAlertModule, NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { GraficasSondasComponent } from './graficas-sondas/graficas-sondas.component';
import { AlertasComponent } from './alertas/alertas.component';
import { AlertasService } from '../servicios/alertas.service';


@NgModule({
  declarations: [
    AppComponent,
    ProgramasActivosComponent,
    ProgramasComponent,
    TelemetriaComponent,
    PrincipalComponent,
    AddProgramaComponent,
    GraficasSondasComponent,
    AlertasComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbPaginationModule,
    HttpClientModule,
    NgbAlertModule,
    FormsModule,
    NgbModule,
    NgMultiSelectDropDownModule.forRoot()
  ],
  providers: [AlertasService],
  bootstrap: [AppComponent],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ]
})

export class AppModule { }
