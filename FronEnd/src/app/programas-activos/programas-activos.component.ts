import { Component, OnInit } from '@angular/core';
import { Programas_Activos, Abonos_Activos, Secuencias_Activos} from '../../modelos/programa-activos';
import { ComunicacionService } from '../../servicios/comunicacion.service';

import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'programas-activos',
  templateUrl: './programas-activos.component.html',
  styleUrls: ['./programas-activos.component.css']
})
export class ProgramasActivosComponent implements OnInit {

  Tab_Programa_activos: Programas_Activos[] = [];
  data_secuencias_activos: Secuencias_Activos[] = [];
  data_abonos_activos: Abonos_Activos[] = [];
  ProgramaActivoVisionado : Programas_Activos;

  page = 1;
  pageSize = 4;
  collectionSize = 0;
  
  listaSecuenciasActivas = [];

  constructor( private programaServicio: ComunicacionService, config: NgbModalConfig, private modalService: NgbModal) {
    config.keyboard = false;
    setInterval(()=> {
      this.getProgramasActivos(); },5000); 
   }

  ngOnInit() {
    this.getProgramasActivos();
  }

  getProgramasActivos(){
    this.programaServicio.getProgramas_Activos().subscribe(res => {
      var Programa_Activo_res  = res as Programas_Activos[];

      /*Actualizacion. Permite la insercion de los nuevo elementos */
      var elementosQueInsertar = Programa_Activo_res.filter(ele => this.Tab_Programa_activos.findIndex(ele2 => ele.id == ele2.id ) == -1);    
      elementosQueInsertar.forEach(progr => {
        this.Tab_Programa_activos.push(progr); progr.Secuencias_Activos.forEach(sec_act => {this.data_secuencias_activos.push(sec_act)})
      });

      this.collectionSize = this.Tab_Programa_activos.length;

      /*Actualizacion. Permite el borrado de los sistemas que hallan finalizado. */
      var elementosQueBorrar = this.Tab_Programa_activos.filter(ele => Programa_Activo_res.findIndex(ele2 => ele.id == ele2.id) == -1);
      elementosQueBorrar.forEach(ele =>{
        ele.Secuencias_Activos.forEach(ele2 => {
          this.data_secuencias_activos.splice(this.data_secuencias_activos.findIndex(ele3 => ele2.idS == ele3.idS),1);
        });
        this.Tab_Programa_activos.splice(this.Tab_Programa_activos.findIndex(pr => ele.id == pr.id));
      });

      
      //Actualizacion. Permite  actualizacion de la variable trascurro tanto de la duracion temporal como del caudal en si//
      Programa_Activo_res.forEach(ele => {
        this.Tab_Programa_activos.find(ele2 => ele.id == ele2.id).orden_secuencia = ele.orden_secuencia;
        ele.Secuencias_Activos.forEach(sec =>{
          var secu = this.data_secuencias_activos.find(eleSec => eleSec.idS == sec.idS);
          secu.estado = sec.estado;
          secu.Trascurrido = sec.Trascurrido;
          secu.Abonos_Activos = sec.Abonos_Activos;
          for(let abo of sec.Abonos_Activos){
            if(sec.estado == 1){
           
              if(this.data_abonos_activos.findIndex(ele =>ele.idA == abo.idA) == -1){
                this.data_abonos_activos.push(abo);
              }
              else{
                var pos = this.data_abonos_activos.findIndex(ele => ele.idA == abo.idA);
                this.data_abonos_activos[pos] = abo;
              }
            }
            else if (sec.estado == 2){
              var pos = this.data_abonos_activos.findIndex(ele => ele.idA == abo.idA);
              if(pos != -1) this.data_abonos_activos.splice(pos,1);
            } 
          }
        })
      });
      
    });

    
  }

  open(content,ProActV) {
    this.ProgramaActivoVisionado = ProActV;
    this.modalService.open(content,  { size: 'lg' });
  }

  get countries(): Programas_Activos[] {
    return this.Tab_Programa_activos
      .map((country, i) => ({id: i + 1, ...country}))
      .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
  }
}
