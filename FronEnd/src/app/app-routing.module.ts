import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PrincipalComponent } from './principal/principal.component';
import { AddProgramaComponent } from './add-programa/add-programa.component';
import { GraficasSondasComponent } from './graficas-sondas/graficas-sondas.component';
import { ConfiguracionComponent} from './configuracion/configuracion.component';
const routes: Routes = [
  {
    path: '',
    component: PrincipalComponent
},
{
  path:'add',
  component: AddProgramaComponent
},
{
  path: 'grafica',
  component: GraficasSondasComponent
},
{
  path: 'configuracion',
  component: ConfiguracionComponent
}

];



@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
