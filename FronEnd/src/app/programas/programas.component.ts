import { Component, OnInit }            from '@angular/core';
import { ComunicacionService }          from '../../servicios/comunicacion.service';
import { AlertasService }               from '../../servicios/alertas.service';
import { programas }                    from '../../modelos/programa';
import { NgbModalConfig, NgbModal }     from '@ng-bootstrap/ng-bootstrap';
import { Router }                       from '@angular/router';

@Component({
  selector: 'programas',
  templateUrl: './programas.component.html',
  styleUrls: ['./programas.component.css']
})
export class ProgramasComponent implements OnInit {
  DiasName : String[]        = [ "Domingo","Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabados"];
  TabPrograma: programas[]   = [];
  ProgramaSelec: programas;

  /* SELECTORES OBJETOS */

  SelectedSecuencia : number = -1;
  SelectedPrograma  : number = -1;


  /* PAGE TAB Y COLLECTION */
  page:any             = 1;
  pageSize:any         = 8;
  conjuntoSize:Number  = 0;
  closeResukt:string   = '';


  constructor(private programaServicio: ComunicacionService, private alert: AlertasService,
    config: NgbModalConfig, private modalService: NgbModal, private ruta: Router) { 
    config.backdrop = 'static';
    
  }


  SelectProg(_id){
    this.SelectedSecuencia = -1;

    if(this.SelectedPrograma != _id) this.SelectedPrograma = _id;
    else this.SelectedPrograma = -1;

  }

  SelectSecu(_id){
    if(this.SelectedSecuencia != _id ) this.SelectedSecuencia = _id;
    else this.SelectedSecuencia = -1;
  }

  get programs(): programas[]{
    return this.TabPrograma
      .map((pr, index) => ({ id: index + 1, ...pr}))
      .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize);
  }

  open(content, ProgramaVision){
    this.ProgramaSelec = ProgramaVision;
    this.modalService.open(content, { size: 'lg'});
  }

  Borrado(programa){
    this.programaServicio.deleteProgram(programa.id).subscribe(res => {
      console.log(res);
      if (res) {
        this.alert.success("Eliminado");
        this.ngOnInit();
      }
    });
  }

  edit(programa){
    this.ruta.navigate(['/add', {id:programa.id, mode: true}]);
  }
  

  ngOnInit(): void {
    this.programaServicio.getPrograma().subscribe(res => {
      this.TabPrograma = res as programas[];
      console.log(this.TabPrograma);
      this.conjuntoSize = this.TabPrograma.length;
    });


  }

}
