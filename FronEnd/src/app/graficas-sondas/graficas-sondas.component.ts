import { Component, OnInit } from '@angular/core';
import { ComunicacionService } from '../../servicios/comunicacion.service';
import { Telemetria_Sensor_G, Telemetria_Sensor_GM, Telemetria_Sensor_Month}  from '../../modelos/telemetria-sensor-grafica';
import { Chart } from 'chart.js';

@Component({
  selector: 'app-graficas-sondas',
  templateUrl: './graficas-sondas.component.html',
  styleUrls: ['./graficas-sondas.component.css']
})
export class GraficasSondasComponent implements OnInit {

  public chart: any = null;
  public chart_month: any = null;

  public lista_Mes  = [{str: "Enero", n:1},{str: "Febrero", n:2},{str: "Marzo", n:3},
    {str: "Abril", n:4},{str: "Mayo", n:5},{str: "Junio", n:6},{str: "Julio", n:7},
    {str: "Agosto", n:8},{str: "Septiembre", n:9},{str: "Octubre", n:10},{str: "Noviembre", n:11},{str: "Diciembre", n:12}];
  
  public lista_Year = [2020,2021];

  public seleciona_mes
  public seleciona_year;

  constructor(private programaServicio: ComunicacionService) { }

  search(){
    this.programaServicio.getDatosMensual(this.seleciona_mes,this.seleciona_year).subscribe(res => {
        
        var l =  res as Telemetria_Sensor_Month[];

        console.log(l);
        this.chart_month = new Chart('chart_month',{
          type: 'bar',
          data:{
            labels : l.map(ele => {
              return ele.date;
            }),

            datasets: [
              {
                
                label: 'TEMP MEDIA',
                fill: false,
                data: l.map(ele =>{return ele.temp;}),
                backgroundColor: '#FF8233',
                borderColor: '#FF8233'
              },
              
              /*{
               
                label: 'TEMP MAX',
                fill: false,
                data: l.map(ele =>{return ele.tempmax;}),
                backgroundColor: '#FF3333',
                borderColor: '#FF3333'
              },

              {
               
                label: 'TEMP MIN',
                fill: false,
                data: l.map(ele =>{return ele.tempmin;}),
                backgroundColor: '#7833FF',
                borderColor: '#7833FF'
              },*/

              {
               
                label: 'HUMEDAD MEDIA',
                fill: false,
                data: l.map(ele =>{return ele.hum;}),
                backgroundColor: '#A3FF33',
                borderColor: '#A3FF33'
              },
              /*
              {
               
                label: 'HUMEDAD MAX',
                fill: false,
                data: l.map(ele =>{return ele.hummax;}),
                backgroundColor: '#37FF33',
                borderColor: '#37FF33'
              },

              {
                
                label: 'HUMEDAD MIN',
                fill: false,
                data: l.map(ele =>{return ele.hummin;}),
                backgroundColor: '#FFDD33',
                borderColor: '#FFDD33'
              }*/

            ],

          },

          options: {
            responsive: true,
            legend: {
              position: 'top',
            },
            title: {
              display: true,
              text: 'Mensual'
            }
          }
        });

    });
   // this.chart_month.update();
  }


  update(element:Telemetria_Sensor_G[]){
    console.log(element.map(ele => {
      return ele.temperatura;
    }));
    this.chart = new Chart('canvas', {
      type: 'line',
      data: {
       labels: element.map(ele => {
        return ele.idData;
      }),
       datasets: [
         {
        label: 'TEMPERATURA',
        fill: false,
        data: element.map(ele => {
          return ele.temperatura;
        }),
        backgroundColor: '#168ede',
        borderColor: '#168ede'
         },
        {
        label: 'HUMEDAD',
        fill: false,
        data: element.map(ele => {
          return ele.humedad;
        }),
        backgroundColor: '#e60000',
        borderColor: '#e60000'
          },
        {
        label: 'HUMEDADT',
        fill: false,
        data: element.map(ele => {
          return(((1024  -Number(ele.humedadT))  / 1024) * 100);
        }),
        backgroundColor: '#8D1F07',
        borderColor: '#8D1F07'
          },
        {
        label: 'VCC',
        fill: false,
        data: element.map(ele => {
          return ele.VCC;
        }),
        backgroundColor: '#00FF15',
        borderColor: '#00FF15'
          }
       ]
        },
        options: {
          responsive: true,
       tooltips: {
        rtl: true,
        intersect:false,
        enabled: true
       },
       legend: {
        display: true,
        position: 'bottom',
        labels: {
         fontColor: 'white'
        }
       },
       scales: {
         yAxes: [{
          display: true,
          ticks: {
           fontColor: "white"
          }
         }],
         xAxes: [{
          type: 'time',
          display: true,
          time: {
              displayFormats: {
                  quarter: 'MMM YYYY'
              }
          },
          ticks: {
          fontColor: "white",
          beginAtZero: false
          }
         }]
       }
        }
     });
  }
  

  upMes(){



  }
  ngOnInit() {
   
    this.programaServicio.getDataG(0).subscribe(res => {
    var elemento = res as Telemetria_Sensor_G[];
    this.update(elemento);
     /*
      
      //this.chart.data.datasets[0].data.push(res.temperatura);
      for (let index = 0; index < elemento.length; index++) {
        this.chart.data.labels.push(elemento[index].idData);
        this.chart.data.datasets[0].data.push(elemento[index].temperatura);
        this.chart.data.datasets[1].data.push(elemento[index].humedad);
        this.chart.data.datasets[2].data.push(((1024  -Number(elemento[index].humedadT))  / 1024) * 100);
        this.chart.data.datasets[3].data.push(elemento[index].VCC);
        

      }
*/

      this.chart.update();
    });

   

  }

}
