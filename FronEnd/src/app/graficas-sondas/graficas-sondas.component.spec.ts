import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GraficasSondasComponent } from './graficas-sondas.component';

describe('GraficasSondasComponent', () => {
  let component: GraficasSondasComponent;
  let fixture: ComponentFixture<GraficasSondasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraficasSondasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GraficasSondasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
