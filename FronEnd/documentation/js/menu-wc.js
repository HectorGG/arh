'use strict';


customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">fron-end documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AlertModule.html" data-type="entity-link">AlertModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link">AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AppModule-5429e2b770de841861429a8848ca2e18"' : 'data-target="#xs-components-links-module-AppModule-5429e2b770de841861429a8848ca2e18"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AppModule-5429e2b770de841861429a8848ca2e18"' :
                                            'id="xs-components-links-module-AppModule-5429e2b770de841861429a8848ca2e18"' }>
                                            <li class="link">
                                                <a href="components/AddProgramaComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AddProgramaComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/AlertasComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AlertasComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/AppComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AppComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/GraficasSondasComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">GraficasSondasComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/PrincipalComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">PrincipalComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ProgramasActivosComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ProgramasActivosComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ProgramasComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ProgramasComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/TelemetriaComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">TelemetriaComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-AppModule-5429e2b770de841861429a8848ca2e18"' : 'data-target="#xs-injectables-links-module-AppModule-5429e2b770de841861429a8848ca2e18"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-AppModule-5429e2b770de841861429a8848ca2e18"' :
                                        'id="xs-injectables-links-module-AppModule-5429e2b770de841861429a8848ca2e18"' }>
                                        <li class="link">
                                            <a href="injectables/AlertasService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>AlertasService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppRoutingModule.html" data-type="entity-link">AppRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/ConfigModule.html" data-type="entity-link">ConfigModule</a>
                            </li>
                </ul>
                </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#components-links"' :
                            'data-target="#xs-components-links"' }>
                            <span class="icon ion-md-cog"></span>
                            <span>Components</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="components-links"' : 'id="xs-components-links"' }>
                            <li class="link">
                                <a href="components/ConfiguracionComponent.html" data-type="entity-link">ConfiguracionComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/GTelemetriaComponent.html" data-type="entity-link">GTelemetriaComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/NaviComponent.html" data-type="entity-link">NaviComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/TelemetriaCaudalComponent.html" data-type="entity-link">TelemetriaCaudalComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/VistaProgramaComponent.html" data-type="entity-link">VistaProgramaComponent</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#classes-links"' :
                            'data-target="#xs-classes-links"' }>
                            <span class="icon ion-ios-paper"></span>
                            <span>Classes</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="classes-links"' : 'id="xs-classes-links"' }>
                            <li class="link">
                                <a href="classes/Abonos.html" data-type="entity-link">Abonos</a>
                            </li>
                            <li class="link">
                                <a href="classes/Abonos-1.html" data-type="entity-link">Abonos</a>
                            </li>
                            <li class="link">
                                <a href="classes/Abonos_Activos.html" data-type="entity-link">Abonos_Activos</a>
                            </li>
                            <li class="link">
                                <a href="classes/Alert.html" data-type="entity-link">Alert</a>
                            </li>
                            <li class="link">
                                <a href="classes/Caudal.html" data-type="entity-link">Caudal</a>
                            </li>
                            <li class="link">
                                <a href="classes/Caudal-1.html" data-type="entity-link">Caudal</a>
                            </li>
                            <li class="link">
                                <a href="classes/CaudalimetroConfig.html" data-type="entity-link">CaudalimetroConfig</a>
                            </li>
                            <li class="link">
                                <a href="classes/PresionConfig.html" data-type="entity-link">PresionConfig</a>
                            </li>
                            <li class="link">
                                <a href="classes/programas.html" data-type="entity-link">programas</a>
                            </li>
                            <li class="link">
                                <a href="classes/programas-1.html" data-type="entity-link">programas</a>
                            </li>
                            <li class="link">
                                <a href="classes/Programas_Activos.html" data-type="entity-link">Programas_Activos</a>
                            </li>
                            <li class="link">
                                <a href="classes/secuencias.html" data-type="entity-link">secuencias</a>
                            </li>
                            <li class="link">
                                <a href="classes/secuencias-1.html" data-type="entity-link">secuencias</a>
                            </li>
                            <li class="link">
                                <a href="classes/Secuencias_Activos.html" data-type="entity-link">Secuencias_Activos</a>
                            </li>
                            <li class="link">
                                <a href="classes/Telemetria_Sensor.html" data-type="entity-link">Telemetria_Sensor</a>
                            </li>
                            <li class="link">
                                <a href="classes/Telemetria_Sensor-1.html" data-type="entity-link">Telemetria_Sensor</a>
                            </li>
                            <li class="link">
                                <a href="classes/Telemetria_Sensor_G.html" data-type="entity-link">Telemetria_Sensor_G</a>
                            </li>
                            <li class="link">
                                <a href="classes/Telemetria_Sensor_G-1.html" data-type="entity-link">Telemetria_Sensor_G</a>
                            </li>
                            <li class="link">
                                <a href="classes/Telemetria_Sensor_GM.html" data-type="entity-link">Telemetria_Sensor_GM</a>
                            </li>
                            <li class="link">
                                <a href="classes/Telemetria_Sensor_GM-1.html" data-type="entity-link">Telemetria_Sensor_GM</a>
                            </li>
                            <li class="link">
                                <a href="classes/Telemetria_Sensor_Month.html" data-type="entity-link">Telemetria_Sensor_Month</a>
                            </li>
                            <li class="link">
                                <a href="classes/Telemetria_Sensor_Month-1.html" data-type="entity-link">Telemetria_Sensor_Month</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#injectables-links"' :
                                'data-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/ComunicacionService.html" data-type="entity-link">ComunicacionService</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/enumerations.html" data-type="entity-link">Enums</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <a data-type="chapter-link" href="routes.html"><span class="icon ion-ios-git-branch"></span>Routes</a>
                        </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});