#include <DHT.h>
#include "LowPower.h"
#include <nRF24L01.h>
#include <printf.h>
#include <RF24.h>
#include <RF24_config.h>



#define CE_PIN 9
#define CSN_PIN 10


const int DHTPin = 3;     // what digital pin we're connected to
const int PINRELE = 5;
#define DHTTYPE DHT22     // DHT 22  (AM2302), AM2321
DHT dht(DHTPin, DHTTYPE);

RF24 radio(CE_PIN, CSN_PIN);

const uint64_t pipes = 0x65646f4e31;
int intentos = 0;
int fac=1;

float hant, tant, humt, vcc;
bool ok = false;

// cambio de escala entre floats
float fmap(float x, float in_min, float in_max, float out_min, float out_max)
{
   return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

void lecturaHumedad(){
  hant = dht.readHumidity();
  tant = dht.readTemperature();
  humt  = analogRead(1);
  vcc  = analogRead(0);
  vcc = fmap(vcc, 0, 1023, 0.0, 25.0);   // cambiar escala a 0.0 - 25.0
}

void enviar(){
  char buffer[10];
  if (isnan(hant) || isnan(tant)){
   return;
  }
  String hum = dtostrf(hant, 1, 4, buffer);
  String tem = dtostrf(tant, 1, 4, buffer);
  String humT = dtostrf(humt, 1, 4, buffer);
  String VCC = dtostrf(vcc, 1, 4, buffer); 
  String datos = String(hum + "," + tem + "," + humT + "," + VCC);
  radio.powerUp();
  
  Serial.println(datos); 
  ok = radio.write(datos.c_str(), datos.length());
  radio.powerDown();
}

void setup() {
  radio.begin();
  Serial.begin(9600); 
  radio.setPALevel(RF24_PA_MAX);
  radio.setChannel(0x76);
  radio.openWritingPipe(pipes);
  pinMode(PINRELE,OUTPUT);

}

void loop() {
 digitalWrite(PINRELE, HIGH); // sets the digital pin 13 on
 delay(5000);
 lecturaHumedad();
 enviar();
 digitalWrite(PINRELE, LOW); // sets the digital pin 13 on
  delay(1000);
 if (intentos < 10 && !ok) {
  intentos++;
  return;
 }
 if(intentos==10) fac=1;
 else fac = 5;
 intentos = 0;
 digitalWrite(PINRELE, LOW); // sets the digital pin 13 on
 for (int i = 0 ;  i  <  16 * fac ; i++)
   LowPower.powerDown(SLEEP_4S, ADC_OFF, BOD_ON);

}
